# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
import unittest
import mock
import rzd
import json
from datetime import datetime, date


class SuggestTests(unittest.TestCase):
    def test_suggest(self):
        with open(os.path.join(os.path.dirname(__file__), 'suggest.json'), 'rb') as f:
            val = json.load(f, encoding='utf8')
        res = rzd._suggest('БЛАГОВЕ', source=mock.Mock(return_value=val))
        expect = [
            {"n":"БЛАГОВЕЩЕНСК","c":2050300,"S":0,"L":4},
            {"n":"БЛАГОВЕЩЕНСКОЕ","c":2000029,"S":4,"L":0},
            {"n":"ВЕЛИКАЯ БЛАГОВЕЩЕНКА","c":2208316,"S":0,"L":3},]
        self.assertEqual(res, expect)


class TrainSearchTests(unittest.TestCase):
    def _mock_session(self, resp_text):
        s = mock.Mock()
        resp = mock.Mock()
        resp.text = resp_text
        s.get.return_value = resp
        s.post.return_value = resp
        return s

    def test_empty(self):
        res_json = '''
        {"result":"OK",
         "TransferSearchMode":"MANUAL",
         "flFPKRoundBonus":false,
         "tp":[
          {"state":"Trains",
           "expressErr":false,
           "msgList":[
            {"message":"Дата отправления находится за пределами периода предварительной продажи",
             "errType":"TICKET_SEARCH_MESSAGE"
             },
            {"message":"Дата отправления находится за пределами периода [N] дней",
             "errType":"TICKET_SEARCH_MESSAGE"
             }
            ],
           "from":"БЛАГОВЕЩЕНСК",
           "where":"БЛАГОВЕЩЕНСКОЕ",
           "date":"01.01.2010",
           "fromCode":2050300,
           "whereCode":2000029,
           "list":[]
           }
          ],
         "discounts":{},
         "tipFlags":{"Ukr":0}
         }
        '''
        s = self._mock_session(res_json)
        station = rzd.Station(name='abc', code=10)
        res = rzd.search_trains(s, station, station, datetime(2010, 1, 1))
        self.assertEqual(len(res.get_trains()), 0)

    def test_single(self):
        res_json = '{"result":"OK","TransferSearchMode":"MANUAL","flFPKRoundBonus":false,"tp":[{"state":"Trains","expressErr":false,"msgList":[],"from":"БЛАГОВЕЩЕНСК","where":"ХАБАРОВСК 1","date":"02.06.2014","fromCode":2050300,"whereCode":2034001,"list":[{"date0":"02.06.2014","date1":"03.06.2014","time0":"13:03","time1":"01:56","trDate0":"02.06.2014","trTime0":"13:03","route0":"БЛАГОВЕЩЕН","route1":"ХАБАРОВС 1","number":"035Ч","number2":"035Ч","elReg":true,"uid":"035CH_1401699780000_1401746160000","train_id":0,"bEntire":true,"carrier":"ФПК","type":0,"station0":"БЛАГОВЕЩЕНСК","station1":"ХАБАРОВСК 1","timeInWay":"12:53","flMsk":3,"cars":[{"freeSeats":10,"itype":6,"servCls":"1Б","tariff":6314,"pt":1890,"typeLoc":"Люкс","type":"Люкс"},{"freeSeats":4,"itype":5,"servCls":"1М","tariff":12388,"pt":3709,"typeLoc":"Мягкий","type":"Мягкий"},{"freeSeats":35,"itype":4,"servCls":"2Л","tariff":3424,"pt":1025,"typeLoc":"Купе","type":"Купе"},{"freeSeats":110,"itype":1,"servCls":"3Э","tariff":2038,"pt":610,"typeLoc":"Плацкартный","type":"Плац"}]}]}],"discounts":{},"tipFlags":{"Ukr":0}}'
        s = self._mock_session(res_json)
        station = rzd.Station(name='abc', code=10)
        res = rzd.search_trains(s, station, station, datetime(2010, 1, 1))
        trains = res.get_trains()
        self.assertEqual(len(trains), 1)
        train = trains[0]
        self.assertEqual(train.departure_datetime(), datetime(2014, 6, 2, 13, 03, 0, 0, rzd.MSK))
        self.assertEqual(train.arrival_datetime(), datetime(2014, 6, 3, 1, 56, 0, 0, rzd.MSK))
        self.assertEqual(train.departure_station(), rzd.Station(name="Благовещенск".upper(), code=2050300))
        self.assertEqual(train.arrival_station(), rzd.Station(name="Хабаровск 1".upper(), code=2034001))
        self.assertEqual(train.number(), "035Ч")
        self.assertEqual(train.el_reg(), True)
        cars = train.cars()
        self.assertEqual(len(cars), 4)
        car = cars[0]
        self.assertEqual(car.name(), 'Люкс')
        self.assertEqual(car.service_cls(), '1Б')
        self.assertEqual(car.free_seats(), 10)
        self.assertEqual(car.tariff(), 6314)
        self.assertEqual(car.points(), 1890)

    def test_search_cars(self):
        res_json = '''
{
  "insuranceCompany": [],
  "lst": [
    {
      "date1": "03.06.2014",
      "date0": "02.06.2014",
      "station0": "\u0411\u041b\u0410\u0413\u041e\u0412\u0415\u0429\u0415\u041d\u0421\u041a",
      "code0": "2050300",
      "code1": "2034001",
      "timeSt1": "",
      "timeSt0": "",
      "time0": "13:03",
      "number": "035\u0427",
      "time1": "01:56",
      "number2": "035\u0427",
      "result": "OK",
      "cars": [
        {
          "specialSeatTypes": "",
          "schemeId": 604,
          "intServiceClass": null,
          "bDeck2": false,
          "conferenceRoomFlag": false,
          "carrierId": 1,
          "clsType": "1\u0411",
          "seats": [
            {
              "type": "dn",
              "free": 10,
              "label": "\u041d\u0438\u0436\u043d\u0438\u0435"
            }
          ],
          "owner": "\u0420\u0416\u0414/\u0414-\u0412",
          "insuranceFlag": false,
          "bVip": false,
          "type": "\u041b\u044e\u043a\u0441",
          "elReg": true,
          "noSmok": false,
          "cnumber": "06",
          "food": false,
          "ctypei": 6,
          "tariffServ": "1199.0",
          "ctype": 6,
          "letter": "\u0427",
          "tariff": "6314.0",
          "tariff2": null,
          "places": "007-016",
          "typeLoc": "\u041b\u044e\u043a\u0441",
          "carrier": "\u0424\u041f\u041a",
          "addSigns": "\u04231"
        },
        {
          "specialSeatTypes": "",
          "schemeId": 321,
          "intServiceClass": null,
          "bDeck2": false,
          "conferenceRoomFlag": false,
          "carrierId": 1,
          "clsType": "1\u041c",
          "seats": [
            {
              "type": "kupe",
              "free": 2,
              "label": "\u041a\u0443\u043f\u0435"
            }
          ],
          "owner": "\u0420\u0416\u0414/\u0414-\u0412",
          "insuranceFlag": false,
          "bVip": true,
          "type": "\u041c\u044f\u0433\u043a\u0438\u0439",
          "elReg": true,
          "noSmok": false,
          "cnumber": "06",
          "food": false,
          "ctypei": 5,
          "tariffServ": "3792.0",
          "ctype": 5,
          "letter": "\u0427",
          "tariff": "12388.4",
          "tariff2": null,
          "places": "001-004",
          "typeLoc": "\u041c\u044f\u0433\u043a\u0438\u0439",
          "carrier": "\u0424\u041f\u041a",
          "addSigns": "\u04231"
        },
        {
          "specialSeatTypes": "",
          "schemeId": 24,
          "intServiceClass": null,
          "bDeck2": false,
          "conferenceRoomFlag": false,
          "carrierId": 1,
          "clsType": "2\u041b",
          "seats": [
            {
              "type": "up",
              "free": 4,
              "label": "\u0412\u0435\u0440\u0445\u043d\u0438\u0435"
            }
          ],
          "owner": "\u0420\u0416\u0414/\u0414-\u0412",
          "insuranceFlag": false,
          "bVip": false,
          "type": "\u041a\u0443\u043f\u0435",
          "elReg": true,
          "noSmok": false,
          "cnumber": "04",
          "food": false,
          "ctypei": 4,
          "tariffServ": "181.0",
          "ctype": 4,
          "letter": "\u0427",
          "tariff": "3424.4",
          "tariff2": null,
          "places": "020,026,028,032",
          "typeLoc": "\u041a\u0443\u043f\u0435",
          "carrier": "\u0424\u041f\u041a",
          "addSigns": ""
        },
        {
          "specialSeatTypes": "",
          "schemeId": 24,
          "intServiceClass": null,
          "bDeck2": false,
          "conferenceRoomFlag": false,
          "carrierId": 1,
          "clsType": "2\u042d",
          "seats": [
            {
              "type": "dn",
              "free": 11,
              "label": "\u041d\u0438\u0436\u043d\u0438\u0435"
            },
            {
              "type": "up",
              "free": 16,
              "label": "\u0412\u0435\u0440\u0445\u043d\u0438\u0435"
            }
          ],
          "owner": "\u0420\u0416\u0414/\u0414-\u0412",
          "insuranceFlag": false,
          "bVip": false,
          "type": "\u041a\u0443\u043f\u0435",
          "elReg": true,
          "noSmok": false,
          "cnumber": "10",
          "food": false,
          "ctypei": 4,
          "tariffServ": "750.0",
          "ctype": 4,
          "letter": "\u0427",
          "tariff": "3993.4",
          "tariff2": null,
          "places": "001-004\u0426,006\u0416,008\u0416,010\u0416,012\u0416,017-024\u0426,026-028\u0416,029-036\u0426",
          "typeLoc": "\u041a\u0443\u043f\u0435",
          "carrier": "\u0424\u041f\u041a",
          "addSigns": "\u041c\u0416 \u04231"
        },
        {
          "specialSeatTypes": "",
          "schemeId": 326,
          "intServiceClass": null,
          "bDeck2": false,
          "conferenceRoomFlag": false,
          "carrierId": 1,
          "clsType": "3\u042d",
          "seats": [
            {
              "type": "dn",
              "free": 3,
              "label": "\u041d\u0438\u0436\u043d\u0438\u0435"
            },
            {
              "type": "up",
              "free": 11,
              "label": "\u0412\u0435\u0440\u0445\u043d\u0438\u0435"
            },
            {
              "type": "ldn",
              "free": 9,
              "label": "\u041d\u0438\u0436\u043d\u0438\u0435&nbsp;\u0431\u043e\u043a\u043e\u0432\u044b\u0435"
            },
            {
              "type": "lup",
              "free": 9,
              "label": "\u0412\u0435\u0440\u0445\u043d\u0438\u0435&nbsp;\u0431\u043e\u043a\u043e\u0432\u044b\u0435"
            }
          ],
          "owner": "\u0420\u0416\u0414/\u0414-\u0412",
          "insuranceFlag": false,
          "bVip": false,
          "type": "\u041f\u043b\u0430\u0446",
          "elReg": true,
          "noSmok": false,
          "cnumber": "11",
          "food": false,
          "ctypei": 1,
          "tariffServ": "116.2",
          "ctype": 3,
          "letter": "\u0427",
          "tariff": "2038.0",
          "tariff2": null,
          "places": "002,004,020,022,024,026,028,030-054",
          "typeLoc": "\u041f\u043b\u0430\u0446\u043a\u0430\u0440\u0442\u043d\u044b\u0439",
          "carrier": "\u0424\u041f\u041a",
          "addSigns": ""
        },
        {
          "specialSeatTypes": "",
          "schemeId": 326,
          "intServiceClass": null,
          "bDeck2": false,
          "conferenceRoomFlag": false,
          "carrierId": 1,
          "clsType": "3\u042d",
          "seats": [
            {
              "type": "dn",
              "free": 2,
              "label": "\u041d\u0438\u0436\u043d\u0438\u0435"
            },
            {
              "type": "up",
              "free": 14,
              "label": "\u0412\u0435\u0440\u0445\u043d\u0438\u0435"
            },
            {
              "type": "ldn",
              "free": 8,
              "label": "\u041d\u0438\u0436\u043d\u0438\u0435&nbsp;\u0431\u043e\u043a\u043e\u0432\u044b\u0435"
            },
            {
              "type": "lup",
              "free": 9,
              "label": "\u0412\u0435\u0440\u0445\u043d\u0438\u0435&nbsp;\u0431\u043e\u043a\u043e\u0432\u044b\u0435"
            }
          ],
          "owner": "\u0420\u0416\u0414/\u0414-\u0412",
          "insuranceFlag": false,
          "bVip": false,
          "type": "\u041f\u043b\u0430\u0446",
          "elReg": true,
          "noSmok": false,
          "cnumber": "12",
          "food": false,
          "ctypei": 1,
          "tariffServ": "116.2",
          "ctype": 3,
          "letter": "\u0427",
          "tariff": "2038.0",
          "tariff2": null,
          "places": "002,004,006,008,014,016,018,020,026,028,030,032-050,052-054",
          "typeLoc": "\u041f\u043b\u0430\u0446\u043a\u0430\u0440\u0442\u043d\u044b\u0439",
          "carrier": "\u0424\u041f\u041a",
          "addSigns": ""
        },
        {
          "specialSeatTypes": "",
          "schemeId": 326,
          "intServiceClass": null,
          "bDeck2": false,
          "conferenceRoomFlag": false,
          "carrierId": 1,
          "clsType": "3\u042d",
          "seats": [
            {
              "type": "dn",
              "free": 3,
              "label": "\u041d\u0438\u0436\u043d\u0438\u0435"
            },
            {
              "type": "up",
              "free": 17,
              "label": "\u0412\u0435\u0440\u0445\u043d\u0438\u0435"
            },
            {
              "type": "ldn",
              "free": 9,
              "label": "\u041d\u0438\u0436\u043d\u0438\u0435&nbsp;\u0431\u043e\u043a\u043e\u0432\u044b\u0435"
            },
            {
              "type": "lup",
              "free": 9,
              "label": "\u0412\u0435\u0440\u0445\u043d\u0438\u0435&nbsp;\u0431\u043e\u043a\u043e\u0432\u044b\u0435"
            }
          ],
          "owner": "\u0420\u0416\u0414/\u0414-\u0412",
          "insuranceFlag": false,
          "bVip": false,
          "type": "\u041f\u043b\u0430\u0446",
          "elReg": true,
          "noSmok": false,
          "cnumber": "13",
          "food": false,
          "ctypei": 1,
          "tariffServ": "116.2",
          "ctype": 3,
          "letter": "\u0427",
          "tariff": "2038.0",
          "tariff2": null,
          "places": "002,004,006,008,010,012,014,016,020,022,024,026-028,030,032-054",
          "typeLoc": "\u041f\u043b\u0430\u0446\u043a\u0430\u0440\u0442\u043d\u044b\u0439",
          "carrier": "\u0424\u041f\u041a",
          "addSigns": ""
        }
      ],
      "route1": "\u0425\u0410\u0411\u0410\u0420\u041e\u0412\u0421 1",
      "route0": "\u0411\u041b\u0410\u0413\u041e\u0412\u0415\u0429\u0415\u041d",
      "type": "\u0421\u041a \u0424\u0418\u0420\u041c",
      "station1": "\u0425\u0410\u0411\u0410\u0420\u041e\u0412\u0421\u041a 1"
    }
  ],
  "schemes": {
  },
  "result": "OK",
  "psaction": null
}
        '''
        s = self._mock_session(res_json)
        station = rzd.Station(name="", code=1000)
        res = rzd.query_seats(s, '', station, station, datetime(2010, 1, 1))
        self.assertEqual(res.frm_dt, datetime(2014, 6, 2, 13, 03, 0, 0, rzd.MSK))
        self.assertEqual(res.to_dt, datetime(2014, 6, 3, 1, 56, 0, 0, rzd.MSK))
        self.assertEqual(res.frm.code, 2050300)
        self.assertEqual(res.to.code, 2034001)
