# -*- coding: utf-8 -*-
""" rzd client module

Module allows you to query for trains, check seats availability,
create ticket order.

Module is using RZD (Российские Железные Дороги) web site: http://pass.rzd.ru/.
"""
from __future__ import unicode_literals
import pytz
from datetime import date, time, datetime
import time
import requests
from pprint import pprint
import json
import os
import re

from six import text_type
from django.utils.http import urlencode, urlquote

_PREFIX = 'http://pass.rzd.ru'
_PREFIXS = 'https://pass.rzd.ru'


_LAYERS = {
    'train_html': 5354,
    'train_api': 5371,
    'car_html': 5372,
    'car_api': 5373,
    'passenger_html': 5374,
    'order_request_html': 5375,
    'order_confirm': 5376,
    'order_status_api': 5378,
}

_STRUCTURE_ID = 735

_DATEFORMAT = '%d.%m.%Y'
_DATETIMEFORMAT = '%d.%m.%Y %H:%M'

# Moscow time zone
MSK = pytz.timezone('Europe/Moscow')


class RzdError(Exception):
    """Base class for errors"""
    pass


class ServerError(RzdError):
    """Internal server error, as opposed to user's error"""
    def __init__(self, message, extra_info=""):
        self.message = message
        self.extra_info = extra_info

    def __str__(self):
        return self.message

    def __unicode__(self):
        return self.message


class LoginError(RzdError):
    """Login error, probably invalid credentials"""
    def __init__(self):
        super(LoginError, self).__init__('LoginError')


def _conv_datetime(date, time):
    """Converts datetime from two strings formatted in Russian format,
    assumed MSK time zone"""
    return datetime.combine(
        datetime.strptime(date, _DATEFORMAT).date(),
        datetime.strptime(time, '%H:%M').time().replace(tzinfo=MSK))


def _getSuggestions(part, lang='ru', compact=False):
    """ Queries server for suggestions by given part

    the part should be 2 upper-cased letters
    """
    assert len(part) == 2, "Should only be called with part len = 2"
    assert part == part.upper(), "part should be uppercased"
    params = {'lang': lang,
              'stationNamePart': part,
              }
    if compact:
        params['compactMode'] = 'y'
    url = _PREFIX + '/suggester?' + urlencode(params)
    r = requests.get(url)
    contents = r.text
    if not contents:
        return []
    try:
        return json.loads(contents)
    except ValueError:
        raise ServerError('Invalid response from server, json expected: ' + contents[:100])


class _SuggestCache(object):
    def __init__(self):
        self._cache = {}

    def get(self, part, source=_getSuggestions):
        if len(part) < 2:
            return []
        elif part in self._cache:
            return self._cache[part]
        else:
            res = self._cache[part] = source(part, compact=True)
            return res


_suggest_cache = _SuggestCache()


class SuggestKey(object):
    """ Keys to suggest dictionaries returned by :func:`suggest`
    """

    #: Name of the station
    name = "n"

    #: Numerical code of the station
    code = "c"


def _suggest(part, source):
    part = part.upper()
    suggest_list = _suggest_cache.get(part[:2], source=source)
    filtered = filter(lambda e: part in e[SuggestKey.name].upper(), suggest_list)
    return sorted(filtered, key=lambda e: e[SuggestKey.name])


def suggest(part):
    """ Returns suggestion list for given station/city

    :param part: Part of the name of the station/city, should be at least
      2 characters long
    :type part: str
    :returns: A list of dictionaries, keys to dictionaries are given
      in :class:`SuggestKey` ordered by station name
    """
    return _suggest(part, _getSuggestions)


class Direction(object):
    """Ticket search direction
    """

    #: Search one way ticket
    one_way = 0

    #: Search round-trip ticket
    with_return = 1


class TrainFlags(object):
    """ Kinds of trains
    """

    #: Long-range trains
    trains = 1

    #: Commuter trains, so called: elektrichki
    commuters = 2


class Station(object):
    """ Represents a station
    """
    def __init__(self, name, code):
        #: Doc name of the station
        self.name = name

        #: Doc numerical code of the station
        self.code = code

    def __eq__(self, other):
        return self.code == other.code

    def __str__(self):
        return "{} ({})".format(self.name, self.code)

    def __unicode__(self):
        return "{} ({})".format(self.name, self.code)

    def __repr__(self):
        return "Station({}, {})".format(repr(self.name), repr(self.code))


class _CarResult(object):
    def __init__(self, car):
        self._car = car

    def name(self):
        """Name of car/class"""
        return self._car['typeLoc']

    def service_cls(self):
        return self._car['servCls']

    def free_seats(self):
        """Number of available seats"""
        return self._car['freeSeats']

    def tariff(self):
        """Tariff per ticket in RUB"""
        return self._car['tariff']

    def points(self):
        """Bonus points"""
        return self._car['pt']


class _TrainResult(object):
    def __init__(self, direction, train, d_datetime, d_station, a_datetime, a_station, cars=[]):
        self._direction = direction
        self._train = train
        self._datetime0 = d_datetime
        self._datetime1 = a_datetime
        self._departure_station = d_station
        self._arrival_station = a_station
        self._cars = cars

    @classmethod
    def parse(cls, direction_data, train_data):
        """ Creates train object from parsed json data
        """

        d_datetime = _conv_datetime(train_data['date0'], train_data['time0'])
        a_datetime = _conv_datetime(train_data['date1'], train_data['time1'])
        d_station = Station(name=direction_data['from'], code=direction_data['fromCode'])
        a_station = Station(name=direction_data['where'], code=direction_data['whereCode'])
        cars = map(_CarResult, train_data['cars'])
        return cls(direction_data, train_data, d_datetime, d_station, a_datetime, a_station, cars=cars)

    def departure_datetime(self):
        """ Returns departure date """
        return self._datetime0

    def arrival_datetime(self):
        """Returns arrival date"""
        return self._datetime1

    def departure_station(self):
        """Departure station, instance of :class:`Station` class"""
        return self._departure_station

    def arrival_station(self):
        """Arrival station, instance of :class:`Station` class"""
        return self._arrival_station

    def departure_route(self):
        return self._train['route0']

    def arrival_route(self):
        return self._train['route1']

    def number(self):
        """Train number"""
        return self._train['number']

    def carrier(self):
        """Carrier's company"""
        return self._train['carrier']

    def el_reg(self):
        """If True electronic registration is possible"""
        return self._train['elReg']

    def cars(self):
        """Returns list of available types of cars/classes"""
        return self._cars


class SearchTrainsResult(object):
    """ Search results

    """
    def __init__(self, res):
        self._res = res
        d = res['tp'][0]
        self._trains = map(lambda tr: _TrainResult.parse(d, tr), d['list'])

    def get_trains(self):
        return self._trains


def search_trains(s, frm, to, dt, ret_dt=None,
                  direction=Direction.one_way,
                  tr_flags=TrainFlags.trains | TrainFlags.commuters,
                  check_seats=True):
    """Queries train options and availability

    :param s: http session
    :type s: requests.Session
    :param frm: from station, should have name and code attributes
    :type frm: :class:`Station`
    :param to: to station, should have name and code attributes
    :type to: :class:`Station`
    :param dt: departure date
    :type dt: date

    :param ret_dt: return date, required when direction equals Direction.with_return
    :type ret_dt: date

    :param direction: can be Direction.one_way or Direction.with_return,
    :type direction: int

    :param tr_flags: kinds of trains to search, is a bit field with possible
      bit flags TrainFlags.trains and TrainFlags.commuters
    :type tr_flags: int

    :param check_seats: if true performs checking of seat availability
    :type check_seats: bool

    :returns: An instance of :class:`SearchTrainsResult` representing search results
    """
    assert frm.code
    assert frm.name
    assert to.code
    assert to.name
    params = {'STRUCTURE_ID': _STRUCTURE_ID,
              'layer_id': _LAYERS['train_api'],
              'dir': direction,
              'tfl': tr_flags,
              'checkSeats': 1 if check_seats else 0,
              'st0': frm.name,
              'code0': frm.code,
              'st1': to.name,
              'code1': to.code,
              'dt0': dt.strftime(_DATEFORMAT),
              }
    if direction == Direction.with_return:
        if ret_dt is None:
            raise ValueError('ret_dt should be specified')
        params['dt1'] = ret_dt.strftime(_DATEFORMAT)
    while True:
        url = '{}/timetable/public/ru?{}'.format(_PREFIX, urlencode(params))
        resp = s.get(url)
        res = json.loads(resp.text)
        if res['result'] != 'RID':
            break
        time.sleep(1)
        params['rid'] = res['rid']
        params['SESSION_ID'] = res['SESSION_ID']
    if res['result'] == 'Error':
        raise ServerError(res['message'], extra_info=res.get('errInfo'))
    if res['tp'][0].get('expressErr', False):
        raise ServerError(res['tp'][0]['msgList'][0]['message'])
    return SearchTrainsResult(res)


class SeatsResult(object):
    """Holds results from :func:`query_seats` call

    Attributes:
     - frm - Departure station
     - frm_dt - Departure date and time
     - to - Destination station
     - to_dt - Arrival time
     - train_number - Train number
     - cars - List of available cars, see car example for details

    Car example:

    .. code-block:: python

      {u'addSigns': u'У1',                  # Additional features of the car, see car features for details
       u'bDeck2': False,
       u'bVip': False,
       u'carrier': u'ФПК',                  # Carier's name
       u'carrierId': 1,
       u'clsType': u'1Б',                   # Car class, see car classes for details
       u'cnumber': u'06',                   # Car number
       u'conferenceRoomFlag': False,
       u'ctype': 6,
       u'ctypei': 6,
       u'elReg': True,                      # Electronic registration is available
       u'food': False,
       u'insuranceFlag': False,
       u'intServiceClass': None,
       u'letter': u'Ч',
       u'noSmok': False,
       u'owner': u'РЖД/Д-В',                # Probably the owner of the car
       u'places': u'007-016,020М,022Ж',     # Available places ranges, suffix can be М-male, Ж-female only.
       u'schemeId': 604,                    # Probably car layout
       u'seats': [                          # List of available seat types
                  {u'free': 10,             # Total number of available seats of this type
                   u'label': u'Нижние',     # Name of the seat type
                   u'type': u'dn'           # Type of the seat, see seat types for details
                   }],
       u'specialSeatTypes': u'',
       u'tariff': u'6314.0',                # Tariff in RUB
       u'tariff2': None,
       u'tariffServ': u'1199.0',
       u'type': u'Люкс',                    # Short name of the car type
       u'typeLoc': u'Люкс'                  # Full name of the car type, e.g. Люкс, Мягкий, Купе, Плацкартный
       }

    Seat types:

    ===== ===============
    Code  Description
    ----- ---------------
    dn    Down seat
    up    Upper seat
    ldn   Side down seat
    lup   Side upper seat
    kupe  Whole room
    ...   possibly others
    ===== ===============


    Car features is a string containing space separated feature codes, e.g.
    МЖ У2. See feature codes for more details.

    Feature codes:

    ========= ============
    Code      Description
    --------- ------------
    МЖ        Car has separate male and female rooms
    У<number> Number of meals per day, e.g. У2 means 2 meals per day
    ...       possibly others
    ========= ============


    Car classes:

    =============== ===========
    *Codes*         *Description*
    --------------- -----------
    1А, 1М, 1Н      Personal room with personal bathroom
    1Б, 1Л          Two persons per room, shared restroom per car
    2Э, 2Л, 2К, 2У  Four persons per room
    3П, 3Э          Platzkart
    ...
    =============== ===========

    """
    @classmethod
    def _parse(cls, data):
        """Parse data dictionary into SeatsResult class"""
        # TODO: support data['lst'][1]
        data = data['lst'][0]
        frm_dt = _conv_datetime(data['date0'], data['time0'])
        to_dt = _conv_datetime(data['date1'], data['time1'])
        frm = Station(name=data['station0'], code=int(data['code0']))
        to = Station(name=data['station1'], code=int(data['code1']))
        res = cls()
        res.frm = frm
        res.to = to
        res.frm_dt = frm_dt
        res.to_dt = to_dt
        res.train_number = data['number']
        res.cars = data['cars']
        return res


def query_seats(s, train_number, frm, to, dt):
    """ Queries given train for available seats

    :param train_number: Train number
    :type train_number: str
    :param frm: Departure station
    :type frm: :class:`Station`
    :param to: Destination station
    :type to: :class:`Station`
    :param dt: Date-time of departure
    :type dt: datetime
    :returns: An instance of :class:`SeatsResult`
    """
    sdate = dt.date().strftime(_DATEFORMAT)
    stime = dt.time().strftime("%H:%M")
    params = {
        'dir': 0,
        'tnum': train_number,
        #'route0': train.departure_route(),
        'st0': frm.name,
        'code0': frm.code,
        'dt0': sdate,
        'time0': stime,
        #'route1': train.arrival_route(),
        'st1': to.name,
        'code1': to.code,
        'dis': '',
        'trDate0': '{} {}'.format(sdate, stime),
        #'bEntire': train._train['bEntire'],
        'tnum0': train_number,
        }
    while True:
        url = '{}/timetable/public/ru?{}'.format(_PREFIX, urlencode({'STRUCTURE_ID': _STRUCTURE_ID, 'layer_id': _LAYERS['car_api']}))
        resp = s.post(url, data=params)
        resp = json.loads(resp.text)
        if resp['result'] != 'RID':
            break
        time.sleep(1)
        params['rid'] = resp['RID']
    return SeatsResult._parse(resp)


# jorney request
# POST https://pass.rzd.ru/ticket/secure/ru?STRUCTURE_ID=735&layer_id=5375&refererLayerId=5374
# form:
# journeyRequest:{"orders":[{"dir":1,"code0":2034000,"code1":2050300,"route0":"ХАБАРОВС 1","route1":"БЛАГОВЕЩЕН","datetime0":"11.06.2014 13:38","number":"035Э","number2":"035Э","letter":"Э","Teema":0,"ctype":1,"cnumber":"12","clsType":"3Э","carrierGroupId":1,"elReg":true,"conferenceRoomFlag":false,"entireCompartmentFlag":0,"carVipFlag":0,"trainDepartureDate":"11.06.2014 13:38 00:00","plBedding":0}],"passengers":[{"lastName":"Денисенко","firstName":"Михаил","midName":"","tariff":"Adult","docType":4,"docNumber":"1111111","gender":2,"country":125,"birthdate":"01.05.2014","birthplace":"Благовещенск"}]}
# response 302


# sending order
# POST https:///ticket/secure/ru?STRUCTURE_ID=735&layer_id=5375&refererLayerId=5374
#   data = journeyRequest ...
#   redirects to https:/ticket/secure/ru?STRUCTURE_ID=735&layer_id=5375&refererLayerId=5374&redirectId=cd093c8d-97c2-417c-93fd-e25f246e23f4
#   page calls POST https:///ticket/secure/ru?STRUCTURE_ID=735&layer_id=5378&refererLayerId=5375
#    data = rid: xxxx
#    responds with json:
#     {"result":"OK","saleOrderId":82322595,"orders":[{"orderId":86719729,"created":"07.05.2014 05:54","dirName":"Forward","code0":2050300,"station0":"БЛАГОВЕЩЕНСК","code1":2000002,"station1":"МОСКВА ЯРОСЛАВСКАЯ","timeInfo":"ВРЕМЯ ОТПР И ПРИБ МОСКОВСКОЕ; ","cost":7904.6,"date0":"16.05.2014","time0":"06:31","route0":null,"route1":null,"number":"079ВА","number2":"079ВА","date1":"22.05.2014","time1":"04:11","cnumber":"15","addSigns":null,"clsType":"3П","type":"Плацкартный","ctype":1,"carrier":"ФПК ЗАБАЙКАЛЬСКИЙ","agent":"ФПК","seatNums":"008","tickets":[{"text":"TicketBean{id=null, ordinal=1, number=73440265831716, reservationNumber=null, passCount=1, seats='008', seatsType='В', tariffType=Полный, tariffInfo='null', tariffB=null, tariffP=null, tariffBP=7904.6, tariffInsurance=0.0, tariffService=0.0, tariffNDS=1205.79, price=7904.6, priceInEuro=null, priceInPoints=null, status=null, code2D='null', registrationChangeDate=null, containsDet5=false, refundTariff=null, refundDate=null, refundInfo='null', refundKrs=null, passengers=[ru.tehnosk.chameleon.layer.express.bean.PassengerBean{id=112290722, index=null, middleName='null', firstName='МИХАИЛ', lastName='ДЕНИСЕНКО', tariffType=null, dateOfBirth=01.05.2014 00:00:00.000, documentType=DocType{id=4, code='ЗЗ', name='Иностранный документ}, documentNumber='111111', citizenship=Country{expressCode=null, isoAlpha3='USA', isForcedRegistrationAllowed=false, localizedNames='{en={name=United States}, ru={name=Соединенные Штаты Америки}}'}, gender=Male, cardNumber='null', loyaltyCardNumber='null', isFullComp=false}], foodChoice={}}","seats":"008","cost":7904.6,"tariff":"Adult","tariffName":"Полный","seatsType":"В","pass":[{"lastName":"ДЕНИСЕНКО","firstName":"МИХАИЛ","docType":4,"docTypeName":"Иностранный документ","docNumber":"111111"}]}]}],"totalSum":7904.6}


# order confirmation:
# GET http:///ticket/secure/ru?STRUCTURE_ID=735&layer_id=5376&refererLayerId=5375&SESSION_ID=&ORDER_ID=86719536
# it redirects to paygate:
# https://paygate.transcredit.ru/index.jsp?ORDERID=49463289&SESSIONID=C99A52F6720E71342779FD9ADCAA3301


def order_request(s, order_req):
    """Sends an order request

    :note: Requires login.

    :param s: Session
    :type s: requests.Session
    :param order_req: Order information dictionary
      e.g.

      .. code-block:: python

        {"orders":
          [
            {"dir":1,   # value 0 doesn't work
             "code0":2034000,
             "code1":2050300,
             "route0":"ХАБАРОВС 1",
             "route1":"БЛАГОВЕЩЕН",
             "datetime0":"11.06.2014 13:38",
             "number":"035Э",
             "number2":"035Э",
             "letter":"Э",
             "Teema":0,
             "ctype":1,
             "cnumber":"12",
             "clsType":"3Э",
             "carrierGroupId":1,
             "elReg":true,
             "conferenceRoomFlag":false,
             "entireCompartmentFlag":0,
             "carVipFlag":0,
             "trainDepartureDate":"11.06.2014 13:38 00:00",
             "plBedding":0
            }
          ],
         "passengers":
           [
             {"lastName":"Doe",
              "firstName":"John",
              "midName":"",
              "tariff":"Adult",
              "docType":4,
              "docNumber":"1111111",
              "gender":2,
              "country":125,
              "birthdate":"01.05.2014",
              "birthplace":"New York"
             }
           ]
        }

    :returns: Order request id which should be checked with :func:`order_status` function.
    """
    params = {'STRUCTURE_ID': _STRUCTURE_ID, 'layer_id': _LAYERS['order_request_html']}
    url = '{}/ticket/secure/ru?{}'.format(_PREFIXS, urlencode(params))
    data = {'journeyRequest': json.dumps(order_req)}
    resp = s.post(url, data)
    rid = re.match(r'.*[^=]=\s*(\d{5,})\b', resp.text, re.S | re.U)
    if not rid and re.match(r".*logon", resp.url):
        raise ValueError("Login required")
    if not rid:
        raise ServerError("rid wasn't found in response " + resp.text)
    return rid.group(1)


def order_status(s, rid):
    """Check the status of order request created by :func:`order_request`

    :param s: Session
    :type s: requests.Session
    :param rid: Order request id
    :returns: Dictionary
      e.g.

      .. code-block:: python

        {"result":"OK",
         "saleOrderId":82322595,
         "orders":
           [
             {"orderId":86719729,
              "created":"07.05.2014 05:54",
              "dirName":"Forward",
              "code0":2050300,
              "station0":"БЛАГОВЕЩЕНСК",
              "code1":2000002,
              "station1":"МОСКВА ЯРОСЛАВСКАЯ",
              "timeInfo":"ВРЕМЯ ОТПР И ПРИБ МОСКОВСКОЕ; ",
              "cost":7904.6,
              "date0":"16.05.2014",
              "time0":"06:31",
              "route0":null,
              "route1":null,
              "number":"079ВА",
              "number2":"079ВА",
              "date1":"22.05.2014",
              "time1":"04:11",
              "cnumber":"15",
              "addSigns":null,
              "clsType":"3П",
              "type":"Плацкартный",
              "ctype":1,
              "carrier":"ФПК ЗАБАЙКАЛЬСКИЙ",
              "agent":"ФПК",
              "seatNums":"008",
              "tickets":
                [
                  {"text": "TicketBean{id=null, ordinal=1, number=73440265831716, reservationNumber=null, passCount=1, seats='008', seatsType='В', tariffType=Полный, tariffInfo='null', tariffB=null, tariffP=null, tariffBP=7904.6, tariffInsurance=0.0, tariffService=0.0, tariffNDS=1205.79, price=7904.6, priceInEuro=null, priceInPoints=null, status=null, code2D='null', registrationChangeDate=null, containsDet5=false, refundTariff=null, refundDate=null, refundInfo='null', refundKrs=null, passengers=[ru.tehnosk.chameleon.layer.express.bean.PassengerBean{id=112290722, index=null, middleName='null', firstName='JOHN', lastName='DOE', tariffType=null, dateOfBirth=01.05.2014 00:00:00.000, documentType=DocType{id=4, code='ЗЗ', name='Иностранный документ}, documentNumber='111111', citizenship=Country{expressCode=null, isoAlpha3='USA', isForcedRegistrationAllowed=false, localizedNames='{en={name=United States}, ru={name=Соединенные Штаты Америки}}'}, gender=Male, cardNumber='null', loyaltyCardNumber='null', isFullComp=false}], foodChoice={}}",
                   "seats":"008",
                   "cost":7904.6,
                   "tariff":"Adult",
                   "tariffName":"Полный",
                   "seatsType":"В",
                   "pass":
                     [
                       {"lastName":"DOE",
                        "firstName":"JOHN",
                        "docType":4,
                        "docTypeName":"Иностранный документ",
                        "docNumber":"111111"
                       }
                     ]
                   }
                ]
              }
           ],
         "totalSum":7904.6
         }
    """
    params = {'STRUCTURE_ID': _STRUCTURE_ID, 'layer_id': _LAYERS['order_status_api']}
    url = '{}/ticket/secure/ru?{}'.format(_PREFIXS, urlencode(params))
    resp = s.post(url, {'rid': rid})
    res = resp.json()
    if res['result'] == 'FAIL':
        raise ServerError("Failed to get order status: " + resp.text)
    return res


def confirm_order(s, order_id):
    params = {
        'STRUCTURE_ID': _STRUCTURE_ID,
        'layer_id': _LAYERS['order_confirm'],
        'ORDER_ID': order_id,
        }
    url = '{}/ticket/secure/ru?{}'.format(_PREFIX, urlencode(params))
    resp = s.get(url)
    print resp.status_code
    print resp.text


def login(s, user, password):
    """ Login user

    User should be registered on https://rzd.ru/selfcare/register

    :param s: Session
    :param user: User name
    :param password: Password
    :raises: :class:`LoginError`
    """
    url = _PREFIXS + '/timetable/j_security_check'
    resp = s.post(url, {'j_username': user, 'j_password': password})
    if re.match('.*logonErr.*', resp.url):
        raise LoginError()


def main():
    #sug = suggest('БЛАГ', lang='ru', compact=True)

    #for st in sug:
    #    print(st)
    frm = {'st': 'БЛАГОВЕЩЕНСК',
           'code': '2050300',
           'dt': date(2014, 06, 06)}

    to = {'st': 'ХАБАРОВСК',
          'code': '2034000',
          'dt': date(2014, 06, 07)}
    s = requests.Session()
    try:
        #req = search_trains(s, frm, to)
        #query_seats(s, req['tp'][0], 0)
        login(s, os.environ['RZDUSER'], os.environ['RZDPASS'])
        #rid = order_request(s, test_order_req)
        rid = 980014219
        print 'rid:', rid
        #order = order_status(s, rid)
        #pprint(order)
        #order_id = order['orders'][0]['orderId']
        order_id = 86723111
        print 'order id:', order_id
        confirm_order(s, order_id)
    except RzdError as err:
        print(text_type(err).encode('utf8'))

if __name__ == '__main__':
    main()
