# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest
import rzd
import requests
import os
from datetime import datetime, date

blagoveschensk = rzd.Station(name="abc", code=2050300)
khabarovsk = rzd.Station(name="sss", code=2034001)


class IntegrTests(unittest.TestCase):
    def test_suggest(self):
        res = rzd.suggest('БЛАГОВЕЩЕНСК')
        self.assertEqual(res[0][rzd.SuggestKey.code], 2050300)

    def test_search_seats(self):
        s = requests.Session()
        res = rzd.search_trains(s, blagoveschensk, khabarovsk, datetime(2014, 6, 2))
        train = res.get_trains()[0]
        res = rzd.query_seats(s, train.number(), blagoveschensk, khabarovsk, train.departure_datetime())

        rzd.login(s, os.environ['RZDUSER'], os.environ['RZDPASS'])
        car = res.cars[0]
        order = {
            "dir": 1,
            "code0": res.frm.code,
            "code1": res.to.code,
            "route0": "abc",
            "route1": "xyz",
            "datetime0": res.frm_dt.strftime(rzd._DATETIMEFORMAT),
            "number": res.train_number,
            "number2": res.train_number,

            "letter": car['letter'],
            "ctype": car['ctype'],
            "cnumber": car['cnumber'],
            "clsType": car['clsType'],
            "elReg": car['elReg'],
            "conferenceRoomFlag": car['conferenceRoomFlag'],
            "entireCompartmentFlag": 0,
            "carVipFlag": car['bVip'],
            "trainDepartureDate": res.frm_dt.strftime(rzd._DATETIMEFORMAT) + " 00:00",
            "plBedding": 0,
        }
        passenger = {
            "lastName": "Doe",
            "firstName": "John",
            "midName": "",
            "tariff": "Adult",
            "docType": 4,
            "docNumber": "111111111",
            "gender": 2,
            "country": 125,
            "birthdate": "01.05.2014",
            "birthplace": "New York",

        }
        order_req = {
            "orders": [order],
            "passengers": [passenger],
        }
        rid = rzd.order_request(s, order_req)
